import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  item = {
    id : new Date(),
    employee :'',
    task : '',
    date : '',
  } 
  items : any[] = []

  constructor(public ar:ActivatedRoute, public gs:GlobalService) { }

  ngOnInit(): void {
    this.items = this.gs.tasks;
    this.ar.queryParams.subscribe((data:any)=>{
      let item = this.items.find(i=>i.id==data.id)
      this.edit_item(item)
    })
  }

  add_to_list(){
    this.item.id = new Date();
    let value = {...this.item}
    this.items.push(value)
    localStorage.setItem('tasks',JSON.stringify(this.items))

  }

  delete_from_list(index:number){
    this.items.splice(index,1)
    localStorage.setItem('tasks',JSON.stringify(this.items))

  }

  edit_item(item:any){
    let value = {...item}
    this.item = value;
  }

  update_in_list(){
    let index = this.items.findIndex(i=>i.id==this.item.id)
    let value = {...this.item}
    this.items[index] = value
    localStorage.setItem('tasks',JSON.stringify(this.items))

  }

}
