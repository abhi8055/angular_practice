import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  form = this.fb.group(
    {
      name: this.fb.control('', [Validators.required, Validators.minLength(3)]),
      gender: '',
      city: '',
      id: new Date()
    }
  )
  constructor(public fb: FormBuilder, public gs: GlobalService, public ar: ActivatedRoute,) { }

  ngOnInit(): void {
    this.items = this.gs.employees;
    this.ar.queryParams.subscribe((data: any) => {
      let item = this.items.find((i: any) => i.id == data.id)
      this.edit_item(item)
    })
  }

  items: any = []

  add_to_list() {

    this.form.value.id = new Date();
    let value = { ...this.form.value }
    this.items.push(value)
    localStorage.setItem('employees', JSON.stringify(this.items))
    this.gs.$show_toast.next('Employee was added')

  }

  update_in_list() {

    let index = this.items.findIndex((i: any) => i.id == this.form.value.id)
    let value = { ...this.form.value }
    this.items[index] = value
    localStorage.setItem('employees',JSON.stringify(this.items))
    this.gs.$show_toast.next('Employee was updated')
    
  }

  edit_item(item: any) {
    let value = { ...item }
    this.form.patchValue(value);
  }

  delete_from_list(index: number) {
    this.items.splice(index, 1)
    localStorage.setItem('employees',JSON.stringify(this.items))
    this.gs.$show_toast.next('Employee was deleted')

  }

}
